#ifndef _user_movements_node_
#define _user_movements_node_


#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "EmotionQuery.h"
#include "IOHelper.h"
#include <ctime>
#include <tf/transform_listener.h>
#include <string>



// [publisher subscriber headers]

// [service client headers]

// [action server client headers]

/**
 * \brief Specific Algorithm Class
 *
 */

const float step_size = 0.3;

class UserMovements
{
  private:

	ros::NodeHandle* nh;
    // [publisher attributes]
	ros::Publisher pub;
	ros::Subscriber sub;

	void gameCallBack(const std_msgs::String::ConstPtr& msg);

	std::clock_t start;
	float time_interval;
    // [subscriber attributes]
	tf::TransformListener listener;
	tf::StampedTransform transform;
	tf::StampedTransform rightHandtransform;
	tf::StampedTransform leftHandtransform;
	tf::StampedTransform headtransform;
	tf::StampedTransform hiptransform;


    // [service attributes]

    // [client attributes]

    // [action server attributes]

    // [action client attributes]

	std::string action;
	float x_user;
	float z_user;


  public:
   /**
    * \brief Constructor
    *
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
	UserMovements(ros::NodeHandle* n);

   /**
    * \brief Destructor
    *
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~UserMovements();

    void getFace();

    void getActions();

    void getUser();

    void getUserPosition();

    int userID;
    void publishAction();

  public:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread();

};

#endif

