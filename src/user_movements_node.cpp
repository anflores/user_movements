#include "../include/user_movements_node.h"

char* key = "Ocp-Apim-Subscription-Key: 81a658f81f154f28a3f4a0220bf7347d";
char* url = "https://api.projectoxford.ai/emotion/v1.0/recognize";

static void print_face(FaceEmotion& f)
{
	std::cout << "Got face with top emotion " << f.get_expression() << "\n" <<
		"\t - Expression: " << expr_label(f.get_expression()) << "\n" <<
		"\t - Position: (" << f.get_top()  << ", " << f.get_left() << ")\n"
		"\t - Dimensions: (" << f.get_height() << ", " << f.get_width() << ")\n" <<
		"\t - Center: (" << f.get_center()[0] << "," << f.get_center()[1] << ")\n" <<
		"\t - Score: " << f.get_score() << std::endl;
}


UserMovements::UserMovements(ros::NodeHandle* n){
	nh = n;
	pub = nh->advertise<std_msgs::String>("kinect_chatter",10);
	sub = nh->subscribe("/python_chatter", 10, &UserMovements::gameCallBack, this);

	time_interval = 4;
	userID = -1;
	for(int i=0;i<10; i++){
		getUser();
		if (userID != -1){
			break;
		}
	}

	getUserPosition();

}

UserMovements::~UserMovements(){
	delete nh;
	nh = NULL;
}

void UserMovements::gameCallBack(const std_msgs::String::ConstPtr& msg){
	std::cout << msg << std::endl;
}

void UserMovements::mainNodeThread(){
	getActions();
}

void UserMovements::publishAction(){
	std_msgs::String msg;
	msg.data = action;
	pub.publish(msg);
}


void UserMovements::getUser(){
	std::string child_fr1("right_hand");
	char child_frame_no1[128];

    ros::Time now = ros::Time::now();
    ros::Duration interval = ros::Duration(1);

	for(int i=0; i<6; i++){
		std::cout << child_frame_no1 << std::endl;
		bool recognized = false;
	    snprintf(child_frame_no1, sizeof(child_frame_no1), "%s_%d", child_fr1.c_str(), i);
		std::cout << child_frame_no1 << std::endl;

	    try{
			listener.waitForTransform("kinect_tf", child_frame_no1, now, interval);
		   	listener.lookupTransform("kinect_tf", child_frame_no1, now, transform);
			recognized = true;
		}
		catch (tf::TransformException ex){
			    ros::Duration(0.01).sleep();
		}
		if(recognized){
			userID = i;
			std::cout << "recognized user: " << i << std::endl;
			break;
		}
	}

}

void UserMovements::getUserPosition(){
	bool hip_r;
	for(int i=0;i<10;i++){
		std::cout << "trying to get position of the hip" << std::endl;
		std::string child_fr1("center_hip");
		char child_frame_no1[128];
		snprintf(child_frame_no1, sizeof(child_frame_no1), "%s_%d", child_fr1.c_str(), userID);
		hip_r = false;
		try{
			listener.lookupTransform("kinect_tf", child_frame_no1, ros::Time(0),hiptransform);
			hip_r = true;
			std::cout << "recognized" << std::endl;
			break;
		}
		catch (tf::TransformException ex){
				ros::Duration(0.01).sleep();
		}
	}
	if(hip_r){
		x_user = hiptransform.getOrigin().getX();
		z_user = hiptransform.getOrigin().getZ();
		std::cout << "got position (x,z): " << x_user << " " << z_user << std::endl;
	}
	else{
		std::cout << "could not get position of the user" << std::endl;

	}
}

void UserMovements::getActions(){
	//listener.lookupTransform("kinect_tf", "/turtle1",  ros::Time(0), transform);
	std::string child_fr1("right_hand");
	char child_frame_no1[128];
    snprintf(child_frame_no1, sizeof(child_frame_no1), "%s_%d", child_fr1.c_str(), userID);
    bool right_hand_r = false;
    try{
	   	listener.lookupTransform("kinect_tf", child_frame_no1, ros::Time(0),rightHandtransform);
	   	right_hand_r = true;
	}
	catch (tf::TransformException ex){
		    ros::Duration(0.01).sleep();
	}

	std::string child_fr2("head");
	char child_frame_no2[128];
    snprintf(child_frame_no2, sizeof(child_frame_no2), "%s_%d", child_fr2.c_str(), userID);
    bool head_r = false;
    try{
	   	listener.lookupTransform("kinect_tf", child_frame_no2, ros::Time(0),headtransform);
	   	head_r = true;
	}
	catch (tf::TransformException ex){
		    ros::Duration(0.01).sleep();
	}

	std::string child_fr3("left_hand");
	char child_frame_no3[128];
    snprintf(child_frame_no3, sizeof(child_frame_no3), "%s_%d", child_fr3.c_str(), userID);
    bool left_hand_r = false;
    try{
	   	listener.lookupTransform("kinect_tf", child_frame_no3, ros::Time(0),leftHandtransform);
	   	left_hand_r = true;
	}
	catch (tf::TransformException ex){
		    ros::Duration(0.01).sleep();
	}

	std::string child_fr4("center_hip");
	char child_frame_no4[128];
	snprintf(child_frame_no4, sizeof(child_frame_no4), "%s_%d", child_fr4.c_str(), userID);
	bool hip_r = false;
	try{
		listener.lookupTransform("kinect_tf", child_frame_no4, ros::Time(0),hiptransform);
		hip_r = true;
		}
	catch (tf::TransformException ex){
		ros::Duration(0.01).sleep();
	}

	if(hip_r){
		float x_hip = hiptransform.getOrigin().getX();
		float z_hip = hiptransform.getOrigin().getZ();
		float diff_x = x_hip - x_user;
		float diff_z = z_hip - z_user;
		if((diff_x < - step_size)&&(action != std::string("walk_right"))){
			action = std::string("walk_right");
			std::cout << "action recognized: walk_right" << std::endl;
			getUserPosition();
			publishAction();

		}
		if((diff_x > step_size)&&(action != std::string("walk_left"))){
			action = std::string("walk_left");
			std::cout << "action recognized: walk_left" << std::endl;
			getUserPosition();
			publishAction();
		}
		if((diff_z <- step_size)&&(action != std::string("walk_forward"))){
			action = std::string("walk_forward");
			std::cout << "action recognized: walk_forward" << std::endl;
			getUserPosition();
			publishAction();
		}
		if((diff_z > step_size)&&(action != std::string("walk_backwards"))){
			action = std::string("walk_backwards");
			std::cout << "action recognized: walk_backwards" << std::endl;
			getUserPosition();
			publishAction();
		}

		//std::cout << "got position (x,z): " << x_hip << " " << z_hip << std::endl;

	}


	if ((head_r)&&(left_hand_r)){
		//std::cout << "y hand" << rightHandtransform.getOrigin().getY() << std::endl;
		//std::cout << "y head" << headtransform.getOrigin().getY() << std::endl << std::endl;
		float y_hand = leftHandtransform.getOrigin().getY();
		float y_head = headtransform.getOrigin().getY();
		if((y_hand > y_head)&(action != std::string("raise_right"))){
			action = std::string("raise_right");
			std::cout << "action recognized: raise_right" << std::endl;
			publishAction();
		}
	}

	if ((head_r)&&(right_hand_r)){
		//std::cout << "y hand" << rightHandtransform.getOrigin().getY() << std::endl;
		//std::cout << "y head" << headtransform.getOrigin().getY() << std::endl << std::endl;
		float y_hand = rightHandtransform.getOrigin().getY();
		float y_head = headtransform.getOrigin().getY();
		if((y_hand > y_head)&(action != std::string("raise_left"))){
			action = std::string("raise_left");
			std::cout << "action recognized: raise_left" << std::endl;
			publishAction();
		}
	}



}

void UserMovements::getFace(){
	float duration = 1000*( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	if (duration > time_interval){
		// Get bytes of file
		const char* file_name = "/home/andres/Descargas/MSEmotionWrapper/image.jpg";
		FileInfo body = read_bytes(file_name);

		// Initialize query
		EmotionQuery query = EmotionQuery(url, key);

		// Send file and gather expressions
		vector<FaceEmotion> emotions = query.send_query(body.content, body.size);

		// List faces
		for (int i = 0; i < emotions.size(); ++i)
		{
			print_face(emotions[i]);
		}
		start = std::clock();
	}
}


int main(int argc, char** argv){
	ros::init(argc, argv,"user_movements_node");
	ros::NodeHandle* nh = new ros::NodeHandle();
	ros::Rate loop_rate(10);
	UserMovements node(nh);
	std::cout << "user_movements_node is running!" << std::endl;

	while (ros::ok()){
		node.mainNodeThread();
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
